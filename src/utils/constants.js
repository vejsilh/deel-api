const CONTRACT_TYPES = {
  new: 'new',
  inProgress: 'in_progress',
  terminated: 'terminated',
};

const PROFILE_TYPES = {
  client: 'client',
  contractor: 'contractor',
};

const HTTP_STATUS_CODE = {
  OK: 200,
  BAD_REQUEST: 400,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  INTERNAL_SERVER: 500,
};

const ERROR_CODES = {
  entityDoesntExist: 100,
  notAllowedToAccess: 101,
  jobAlreadyPaid: 102,
  notEnoughFunds: 103,
  dbError: 104,
  valueOutOfBounds: 105,
  requestSchemaInvalid: 106,
  somethingWentWrong: 107,
};

const DEFUALT_BEST_CLIENTS_LIMIT = 2;

const MAX_DEPOSIT = 0.25;

module.exports = {
  CONTRACT_TYPES,
  PROFILE_TYPES,
  HTTP_STATUS_CODE,
  ERROR_CODES,
  MAX_DEPOSIT,
  DEFUALT_BEST_CLIENTS_LIMIT,
};
