const bunyan = require('bunyan');

const logger = bunyan.createLogger({
  name: 'deel-backend',
  stream: process.stdout,
  level: 'info',
  serializers: bunyan.stdSerializers,
});

module.exports = logger;
