const Joi = require('joi');

const balancesSchemas = {
  depositMoney: {
    body: {
      amount: Joi.number().positive().required(),
    },
    params: {
      userId: Joi.number().integer().min(0).required(),
    },
  },
};

const contractsSchemas = {
  getById: {
    params: {
      id: Joi.number().integer().min(0).required(),
    },
  },
};

const jobsSchemas = {
  pay: {
    params: {
      jobId: Joi.number().integer().min(0).required(),
    },
  },
};

const adminSchemas = {
  getBestProfession: {
    query: {
      start: Joi.date().required(),
      end: Joi.date().required(),
    },
  },
  getBestClients: {
    query: {
      start: Joi.date().required(),
      end: Joi.date().required(),
      limit: Joi.number().integer().min(1),
    },
  },
};

module.exports = {
  balancesSchemas, contractsSchemas, jobsSchemas, adminSchemas,
};
