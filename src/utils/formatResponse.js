const { HTTP_STATUS_CODE, ERROR_CODES } = require('./constants');
const ResponseError = require('./responseError');

const formatResponse = (res, data) => {
  if (data instanceof ResponseError) {
    return res.status(data.httpCode).json({
      error_code: data.errorCode,
      message: data.message,
    });
  } if (data instanceof Error) {
    return res.status(HTTP_STATUS_CODE.BAD_REQUEST).json({
      error_code: ERROR_CODES.somethingWentWrong,
      message: data.message,
    });
  }

  return res.status(HTTP_STATUS_CODE.OK).json(data);
};

module.exports = { formatResponse };
