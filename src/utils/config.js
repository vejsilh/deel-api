const dotenv = require('dotenv');

class Config {
  constructor() {
    const envFile = process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : '.env';
    dotenv.config({ path: envFile });

    this.dbDialect = process.env.DB_DIALECT;
    this.dbPath = process.env.DB_PATH;
    this.dbLogging = process.env.DB_LOGGING === 'true';
  }
}

module.exports = new Config();
