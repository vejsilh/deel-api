class ResponseError extends Error {
  constructor(httpCode, errorCode, description) {
    super(description);
    Object.setPrototypeOf(this, new.target.prototype);

    this.httpCode = httpCode;
    this.errorCode = errorCode;

    Error.captureStackTrace(this);
  }
}

module.exports = ResponseError;
