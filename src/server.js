const app = require('./app');
const logger = require('./utils/logger');

async function init() {
  try {
    app.listen(3001, () => {
      logger.info('Express App Listening on Port 3001');
    });
    process
      .on('unhandledRejection', (reason, p) => {
        logger.error({ reason, p }, 'Unhandled Rejection at Promise');
      })
      .on('uncaughtException', (err) => {
        logger.error({ err }, 'Uncaught Exception thrown');
        process.exit(1);
      });
  } catch (err) {
    logger.error({ err }, 'An error occurred.');
    process.exit(1);
  }
}

init();
