const express = require('express');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const { sequelize } = require('./db');
const routes = require('./routes');
const swagerDocument = require('../swagger.json');
const { checkSchemaErrors } = require('./middleware/checkSchemaErrors');

const app = express();
app.use(bodyParser.json());
app.set('sequelize', sequelize);
app.set('models', sequelize.models);

app.use(routes);
app.use(checkSchemaErrors);
app.use(
  '/api-docs',
  swaggerUi.serve,
);
app.use(
  '/api-docs',
  swaggerUi.setup(swagerDocument),
);

module.exports = app;
