const ContractsService = require('../services/contracts.service');
const { formatResponse } = require('../utils/formatResponse');
const logger = require('../utils/logger');

class ContractsController {
  constructor() {
    this.contractsService = new ContractsService();
    this.getById = this.getById.bind(this);
    this.getAll = this.getAll.bind(this);
  }

  async getById(req, res) {
    try {
      const { id } = req.params;
      const contract = await this.contractsService.findById(id, req.profile.id);
      return formatResponse(res, contract);
    } catch (err) {
      logger.error({ req, err }, 'Get Contract By Id Error');
      return formatResponse(res, err);
    }
  }

  async getAll(req, res) {
    try {
      const contracts = await this.contractsService.findAllUsersContracts(req.profile.id);

      return formatResponse(res, contracts);
    } catch (err) {
      logger.error({ req, err }, 'Get All Contracts Error');
      return formatResponse(res, err);
    }
  }
}

module.exports = new ContractsController();
