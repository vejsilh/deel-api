const JobsService = require('../services/jobs.service');
const ProfileService = require('../services/profile.service');
const { formatResponse } = require('../utils/formatResponse');
const logger = require('../utils/logger');

class ProfileController {
  constructor() {
    this.profileService = new ProfileService();
    this.jobsService = new JobsService();
    this.depositMoney = this.depositMoney.bind(this);
  }

  async depositMoney(req, res) {
    try {
      const { userId } = req.params;
      const { amount } = req.body;
      const unpaidJobs = await this.jobsService.findAllUsersUnpaidJobs(userId);
      const profile = await this.profileService.deposit(userId, amount, unpaidJobs);
      return formatResponse(res, profile);
    } catch (err) {
      logger.error({ req, err }, 'Get Best Profession Error');
      return formatResponse(res, err);
    }
  }
}

module.exports = new ProfileController();
