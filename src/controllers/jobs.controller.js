const JobsService = require('../services/jobs.service');
const { formatResponse } = require('../utils/formatResponse');
const logger = require('../utils/logger');

class JobsController {
  constructor() {
    this.jobsService = new JobsService();
    this.unpaid = this.unpaid.bind(this);
    this.pay = this.pay.bind(this);
  }

  async unpaid(req, res) {
    try {
      const jobs = await this.jobsService.findAllUsersUnpaidJobs(req.profile.id);
      return formatResponse(res, jobs);
    } catch (err) {
      logger.error({ req, err }, 'Get Unpaid Jobs Error');
      return formatResponse(res, err);
    }
  }

  async pay(req, res) {
    try {
      const { jobId } = req.params;
      await this.jobsService.payJob(jobId, req.profile.id);
      return formatResponse(res, { paid: true });
    } catch (err) {
      logger.error({ req, err }, 'Pay For the Job Error');
      return formatResponse(res, err);
    }
  }
}

module.exports = new JobsController();
