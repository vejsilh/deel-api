const AdminService = require('../services/admin.service');
const { formatResponse } = require('../utils/formatResponse');
const logger = require('../utils/logger');

class AdminController {
  constructor() {
    this.adminService = new AdminService();
    this.getBestProfession = this.getBestProfession.bind(this);
    this.getBestClients = this.getBestClients.bind(this);
  }

  async getBestProfession(req, res) {
    try {
      const { start, end } = req.query;
      const bestProfession = await this.adminService.findBestProfession(start, end);

      return formatResponse(res, { best_profession: bestProfession });
    } catch (err) {
      logger.error({ req, err }, 'Get Best Profession Error');
      return formatResponse(res, err);
    }
  }

  async getBestClients(req, res) {
    try {
      const { start, end, limit } = req.query;
      const clients = await this.adminService.findHighestPayingClients(start, end, limit);

      return formatResponse(res, clients);
    } catch (err) {
      logger.error({ req, err }, 'Get Best Clients Error');
      return formatResponse(res, err);
    }
  }
}

module.exports = new AdminController();
