const { BaseRepository } = require('./base.repository');

class ProfileRepository extends BaseRepository {
  findOne(options) {
    return this.db.Profile.findOne(options);
  }
}

module.exports = { ProfileRepository };
