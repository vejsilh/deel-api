const { BaseRepository } = require('./base.repository');

class JobsRepository extends BaseRepository {
  findOne(options) {
    return this.db.Job.findOne(options);
  }

  findAll(options) {
    return this.db.Job.findAll(options);
  }
}

module.exports = { JobsRepository };
