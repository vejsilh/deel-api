const { BaseRepository } = require('./base.repository');

class ContractsRepository extends BaseRepository {
  findOne(options) {
    return this.db.Contract.findOne(options);
  }

  findAll(options) {
    return this.db.Contract.findAll(options);
  }
}

module.exports = { ContractsRepository };
