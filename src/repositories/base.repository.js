class BaseRepository {
  constructor(db, sequelize) {
    this.db = db;
    this.sequelize = sequelize;
  }
}

module.exports = { BaseRepository };
