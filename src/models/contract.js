const { Sequelize } = require('sequelize');
const { CONTRACT_TYPES } = require('../utils/constants');

class Contract extends Sequelize.Model {}

const initializeContract = (sequelize) => Contract.init(
  {
    terms: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    status: {
      type: Sequelize.ENUM(Object.values(CONTRACT_TYPES)),
    },
  },
  {
    sequelize,
    modelName: 'Contract',
    paranoid: true,
  },
);

module.exports = {
  Contract,
  initializeContract,
};
