const Sequelize = require('sequelize');
const { PROFILE_TYPES } = require('../utils/constants');

class Profile extends Sequelize.Model {}

const initializeProfile = (sequelize) => Profile.init(
  {
    firstName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    profession: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    balance: {
      type: Sequelize.DECIMAL(12, 2),
    },
    type: {
      type: Sequelize.ENUM(Object.values(PROFILE_TYPES)),
    },
  },
  {
    sequelize,
    modelName: 'Profile',
    paranoid: true,
  },
);

module.exports = {
  Profile,
  initializeProfile,
};
