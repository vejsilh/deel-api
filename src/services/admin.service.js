const { Op } = require('sequelize');
const { models, sequelize } = require('../db');
const { Contract } = require('../models/contract');
const { Profile } = require('../models/profile');
const { JobsRepository } = require('../repositories/jobs.repository');
const { DEFUALT_BEST_CLIENTS_LIMIT } = require('../utils/constants');

class AdminService {
  constructor() {
    this.jobsRepository = new JobsRepository(models, sequelize);
  }

  /**
   *
   * @param {Date} startDate  - start filter date
   * @param {Date} endDate  - end filter date
   * @returns {Promise<string|null>} Promise object represents best profession
   */
  async findBestProfession(startDate, endDate) {
    const jobs = await this.jobsRepository.findAll({
      attributes: [[this.jobsRepository.sequelize.fn('sum', this.jobsRepository.sequelize.col('Job.price')), 'total']],
      where: {
        paid: true,
        paymentDate: {
          [Op.between]: [startDate, endDate],
        },
      },
      include: [{
        model: Contract,
        attributes: ['id'],
        include: [
          {
            model: Profile,
            as: 'Contractor',
            attributes: ['profession'],
          },
        ],
      }],
      order: [[this.jobsRepository.sequelize.col('total'), 'DESC']],
      group: ['Contract.Contractor.profession'],
    });

    if (jobs.length) {
      const bestProfession = jobs[0].Contract.Contractor.profession;
      return bestProfession;
    }
    return null;
  }

  /**
   *
   * @param {Date} startDate  - start filter date
   * @param {Date} endDate  - end filter date
   * @param {integer} limit  - number of records
   * @returns {Promise<Client[]>} Promise object represents highest paying clients
   */
  async findHighestPayingClients(startDate, endDate, limit = DEFUALT_BEST_CLIENTS_LIMIT) {
    const jobs = await this.jobsRepository.findAll({
      attributes: [[this.jobsRepository.sequelize.fn('sum', this.jobsRepository.sequelize.col('Job.price')), 'paid']],
      where: {
        paid: true,
        paymentDate: {
          [Op.between]: [startDate, endDate],
        },
      },
      include: [{
        model: Contract,
        attributes: ['id'],
        include: [
          {
            model: Profile,
            as: 'Client',
            attributes: ['id', 'firstName', 'lastName'],
          },
        ],
      }],
      order: [[this.jobsRepository.sequelize.col('paid'), 'DESC']],
      group: ['Contract.Client.id'],
      limit,
    });

    const clients = jobs.map((job) => ({
      id: job.Contract.Client.id,
      fullName: `${job.Contract.Client.firstName} ${job.Contract.Client.lastName}`,
      paid: job.paid,
    }));

    return clients;
  }
}

module.exports = AdminService;
