const { models, sequelize } = require('../db');
const { ProfileRepository } = require('../repositories/profile.repository');
const { HTTP_STATUS_CODE, ERROR_CODES, MAX_DEPOSIT } = require('../utils/constants');
const ResponseError = require('../utils/responseError');

class ProfileService {
  constructor() {
    this.profileRepository = new ProfileRepository(models, sequelize);
  }

  /**
   *
   * @param {number} userId  - user id
   * @param {number} amount  - amount to be deposited
   * @param {[Job]} unpaidJobs  - user's unpaid jobs
   * @returns {Promise<[Job]>} Promise object represents profile
   */
  async deposit(userId, amount, unpaidJobs) {
    const sumOfUnpaid = unpaidJobs.reduce((acc, job) => acc + job.price, 0);

    if (amount > (sumOfUnpaid * MAX_DEPOSIT)) {
      throw new ResponseError(HTTP_STATUS_CODE.BAD_REQUEST, ERROR_CODES.valueOutOfBounds, "Can't deposit more than 25% of unpaid jobs");
    }

    const profile = await this.profileRepository.findOne({ where: { id: userId } });
    profile.balance += amount;
    await profile.save();
    return profile;
  }
}

module.exports = ProfileService;
