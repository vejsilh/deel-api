const { Op } = require('sequelize');
const { models, sequelize } = require('../db');
const { ContractsRepository } = require('../repositories/contracts.repository');
const { CONTRACT_TYPES, HTTP_STATUS_CODE, ERROR_CODES } = require('../utils/constants');
const ResponseError = require('../utils/responseError');

class ContractsService {
  constructor() {
    this.contractsRepository = new ContractsRepository(models, sequelize);
  }

  /**
   *
   * @param {number} id  - contract id
   * @param {number} userId  - id of a user
   * @returns {Promise<Contract>} Promise object represents contract
   */
  async findById(id, userId) {
    const contract = await this.contractsRepository.findOne({ where: { id } });

    if (!contract) {
      throw new ResponseError(HTTP_STATUS_CODE.NOT_FOUND, ERROR_CODES.entityDoesntExist, "Contract doesn't exist");
    }
    if (contract.ClientId !== userId
      && contract.ContractorId !== userId) {
      throw new ResponseError(HTTP_STATUS_CODE.FORBIDDEN, ERROR_CODES.notAllowedToAccess, "Contract doesn't belong to user");
    }

    return contract;
  }

  /**
   *
   * @param {number} id  - user id
   * @returns {Promise<[Contract]>} Promise object represents user's contract
   */
  findAllUsersContracts(id) {
    return this.contractsRepository.findAll({
      where: {
        status: {
          [Op.not]: CONTRACT_TYPES.terminated,
        },
        [Op.or]: [{ ClientId: id }, { ContractorId: id }],
      },
    });
  }
}

module.exports = ContractsService;
