const { Op } = require('sequelize');
const { models, sequelize } = require('../db');
const { Contract } = require('../models/contract');
const { JobsRepository } = require('../repositories/jobs.repository');
const { ProfileRepository } = require('../repositories/profile.repository');
const { CONTRACT_TYPES, HTTP_STATUS_CODE, ERROR_CODES } = require('../utils/constants');
const ResponseError = require('../utils/responseError');

class JobsService {
  constructor() {
    this.jobsRepository = new JobsRepository(models, sequelize);
    this.profileRepository = new ProfileRepository(models, sequelize);
  }

  /**
   *
   * @param {number} id  - user id
   * @returns {Promise<[Job]>} Promise object represents user's jobs
   */
  findAllUsersUnpaidJobs(id) {
    return this.jobsRepository.findAll({
      where: {
        paid: null,
      },
      include: {
        model: Contract,
        attributes: [],
        where: {
          status: {
            [Op.not]: CONTRACT_TYPES.terminated,
          },
          [Op.or]: [{ ClientId: id }, { ContractorId: id }],
        },
      },
    });
  }

  /**
   *
   * @param {number} jobId  - job id
   * @param {number} profileId  - user id
   * @returns {Promise<boolean>} Promise object represents boolean value in case of success
   */
  async payJob(jobId, profileId) {
    const [job, profile] = await Promise.all([this.jobsRepository.findOne({
      where: { id: jobId },
      include: { model: Contract },
    }), this.profileRepository.findOne({ where: { id: profileId } })]);

    if (!job) {
      throw new ResponseError(HTTP_STATUS_CODE.NOT_FOUND, ERROR_CODES.entityDoesntExist, "Job doesn't exist");
    }

    if (job.Contract.status === CONTRACT_TYPES.terminated || job.Contract.ClientId !== profileId) {
      throw new ResponseError(HTTP_STATUS_CODE.FORBIDDEN, ERROR_CODES.notAllowedToAccess, "Doesn't have permission to pay this job");
    }

    if (job.paid) {
      throw new ResponseError(HTTP_STATUS_CODE.BAD_REQUEST, ERROR_CODES.jobAlreadyPaid, 'Job already paid');
    }

    if (profile.balance < job.price) {
      throw new ResponseError(HTTP_STATUS_CODE.BAD_REQUEST, ERROR_CODES.notEnoughFunds, 'Not enough funds to pay for the job');
    }

    const transaction = await this.jobsRepository.sequelize.transaction();
    try {
      job.paid = true;
      job.paymentDate = new Date();
      profile.balance -= job.price;
      await profile.save({ transaction });
      await job.save({ transaction });
      await transaction.commit();
      return true;
    } catch (err) {
      await transaction.rollback();
      throw new ResponseError(HTTP_STATUS_CODE.INTERNAL_SERVER, ERROR_CODES.dbError, 'Unexpected database error');
    }
  }
}

module.exports = JobsService;
