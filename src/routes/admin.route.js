const router = require('express').Router();
const expressJoi = require('express-joi-validator');
const adminController = require('../controllers/admin.controller');
const { adminSchemas } = require('../utils/schemaValidators');

router.get('/best-profession', expressJoi(adminSchemas.getBestProfession), adminController.getBestProfession);
router.get('/best-clients', expressJoi(adminSchemas.getBestClients), adminController.getBestClients);

module.exports = router;
