const router = require('express').Router();
const expressJoi = require('express-joi-validator');
const jobsController = require('../controllers/jobs.controller');
const { jobsSchemas } = require('../utils/schemaValidators');

router.get('/unpaid', jobsController.unpaid);
router.post('/:jobId/pay', expressJoi(jobsSchemas.pay), jobsController.pay);

module.exports = router;
