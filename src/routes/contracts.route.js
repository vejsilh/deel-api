const router = require('express').Router();
const expressJoi = require('express-joi-validator');
const contractsController = require('../controllers/contracts.controller');
const { contractsSchemas } = require('../utils/schemaValidators');

router.get('/:id', expressJoi(contractsSchemas.getById), contractsController.getById);
router.get('/', contractsController.getAll);

module.exports = router;
