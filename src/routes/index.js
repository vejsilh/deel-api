const router = require('express').Router();
const { getProfile } = require('../middleware/getProfile');
const contractsRoutes = require('./contracts.route');
const jobsRoutes = require('./jobs.route');
const balancesRoutes = require('./balances.route');
const adminRoutes = require('./admin.route');

router.use('/contracts', getProfile, contractsRoutes);
router.use('/jobs', getProfile, jobsRoutes);
router.use('/balances', getProfile, balancesRoutes);
router.use('/admin', getProfile, adminRoutes);

module.exports = router;
