const router = require('express').Router();
const expressJoi = require('express-joi-validator');
const profileController = require('../controllers/profile.controller');
const { balancesSchemas } = require('../utils/schemaValidators');

router.post('/deposit/:userId', expressJoi(balancesSchemas.depositMoney), profileController.depositMoney);

module.exports = router;
