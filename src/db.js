const Sequelize = require('sequelize');
const { Contract, initializeContract } = require('./models/contract');
const { Job, initializeJob } = require('./models/job');
const { Profile, initializeProfile } = require('./models/profile');
const config = require('./utils/config');

const sequelize = new Sequelize({
  dialect: config.dbDialect,
  storage: config.dbPath,
  logging: config.dbLogging,
});

initializeProfile(sequelize);
initializeContract(sequelize);
initializeJob(sequelize);

Profile.hasMany(Contract, { as: 'Contractor', foreignKey: 'ContractorId' });
Contract.belongsTo(Profile, { as: 'Contractor' });
Profile.hasMany(Contract, { as: 'Client', foreignKey: 'ClientId' });
Contract.belongsTo(Profile, { as: 'Client' });
Contract.hasMany(Job);
Job.belongsTo(Contract);

const models = {
  Profile,
  Contract,
  Job,
};

module.exports = {
  sequelize,
  models,
};
