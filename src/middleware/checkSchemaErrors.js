const { HTTP_STATUS_CODE, ERROR_CODES } = require('../utils/constants');
const { formatResponse } = require('../utils/formatResponse');
const ResponseError = require('../utils/responseError');

const checkSchemaErrors = (err, req, res, next) => {
  if (err.isBoom) {
    const responseErr = new ResponseError(
      HTTP_STATUS_CODE.BAD_REQUEST,
      ERROR_CODES.requestSchemaInvalid,
      err.output.payload.message,
    );
    return formatResponse(res, responseErr);
  }
  return next();
};

module.exports = { checkSchemaErrors };
