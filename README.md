# DEEL BACKEND TASK

## Running the app locally
It's recommended to use NodeJs version `10.x` (because of some old packages). After running `npm install`, you can run the app using `npm start`. It uses environment variables, which must be defined in `.env` file. 
Example of `.env` contents:
```
DB_DIALECT="sqlite"
DB_PATH="./database.sqlite3"
DB_LOGGING=false
```

If you start app for the first time, make sure to create and populate the database by running `npm run seed`.

## Running the tests
Tests use `test` environment, therefore they require you to have `.env.test` file with variables defined in it. To run the tests execute `npm run test`.
Example of `.env.test` contents:
```
DB_DIALECT="sqlite"
DB_PATH="./database-test.sqlite3"
DB_LOGGING=false
```

Check package.json for full list of available commands.

## REST API Documentation
API documentation is avaiable after you start the server. Visit [http://localhost:3001/api-docs/](http://localhost:3001/api-docs/). The page will render Swagger UI with all endpoints documented and ready for try.
