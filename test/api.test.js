const supertest = require('supertest');
const app = require('../src/app');
const { ERROR_CODES } = require('../src/utils/constants');

const request = supertest(app);

describe('API Test', () => {
  test('GET /contracts - success', async () => {
    const res = await request
      .get('/contracts')
      .set({ profile_id: 1 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body).toHaveLength(1);
    expect(res.body[0].id).toBe(2);
    expect(res.body[0].status).toBe('in_progress');
    expect(res.body[0].ClientId).toBe(1);
  });

  test('GET /contracts - unauthorized error', async () => {
    const res = await request
      .get('/contracts')
      .send();

    expect(res.status).toBe(401);
  });

  test('GET /contracts/:id - success', async () => {
    const res = await request
      .get('/contracts/2')
      .set({ profile_id: 1 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body.id).toBe(2);
    expect(res.body.ClientId).toBe(1);
  });

  test('GET /contracts/:id - unauthorized error', async () => {
    const res = await request
      .get('/contracts/2')
      .set({ profile_id: 5 })
      .send();

    expect(res.status).toBe(403);
  });

  test('GET /contracts/:id - not found error', async () => {
    const res = await request
      .get('/contracts/100000000')
      .set({ profile_id: 5 })
      .send();

    expect(res.status).toBe(404);
  });

  test('GET /contracts/:id - param id error', async () => {
    const res = await request
      .get('/contracts/2sd3')
      .set({ profile_id: 5 })
      .send();

    expect(res.status).toBe(400);
    expect(res.body.error_code).toBe(ERROR_CODES.requestSchemaInvalid);
  });

  test('GET /jobs/unpaid - success', async () => {
    const res = await request
      .get('/jobs/unpaid')
      .set({ profile_id: 1 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body).toHaveLength(1);
    expect(res.body[0].paid).toBeNull();
  });

  test('GET /jobs/unpaid - unauthorized error', async () => {
    const res = await request
      .get('/jobs/unpaid')
      .send();

    expect(res.status).toBe(401);
  });

  test('POST /jobs/:id/pay - success', async () => {
    const res = await request
      .post('/jobs/2/pay')
      .set({ profile_id: 1 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body).toEqual({ paid: true });
  });

  test('POST /jobs/:id/pay - not found error', async () => {
    const res = await request
      .post('/jobs/1523525/pay')
      .set({ profile_id: 1 })
      .send();

    expect(res.status).toBe(404);
    expect(res.body.error_code).toEqual(ERROR_CODES.entityDoesntExist);
    expect(res.body.message).toEqual("Job doesn't exist");
  });

  test('POST /jobs/:id/pay - not allowed to pay error', async () => {
    const res = await request
      .post('/jobs/1/pay')
      .set({ profile_id: 1 })
      .send();

    expect(res.status).toBe(403);
    expect(res.body.error_code).toEqual(ERROR_CODES.notAllowedToAccess);
    expect(res.body.message).toEqual("Doesn't have permission to pay this job");
  });

  test('POST /jobs/:id/pay - already paid error', async () => {
    const res = await request
      .post('/jobs/10/pay')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(400);
    expect(res.body.error_code).toEqual(ERROR_CODES.jobAlreadyPaid);
    expect(res.body.message).toEqual('Job already paid');
  });

  test('POST /balances/deposit/:user_id - success', async () => {
    const res = await request
      .post('/balances/deposit/2')
      .set({ profile_id: 3 })
      .send({
        amount: 100,
      });

    expect(res.status).toBe(200);
    expect(res.body.balance).toBeGreaterThan(0);
    expect(res.body.id).toBe(2);
  });

  test('POST /balances/deposit/:user_id - body amount error', async () => {
    const res = await request
      .post('/balances/deposit/2')
      .set({ profile_id: 3 })
      .send({ amount: 'should be number' });

    expect(res.status).toBe(400);
    expect(res.body.error_code).toBe(ERROR_CODES.requestSchemaInvalid);
  });

  test('POST /admin/best-profession - success', async () => {
    const res = await request
      .get('/admin/best-profession?start=2018-01-01T01:01:01.0001Z&end=2023-11-01T01:01:01.0001Z')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body.best_profession).toBe('Programmer');
  });

  test('POST /admin/best-profession - no result success', async () => {
    const res = await request
      .get('/admin/best-profession?start=2025-01-01T01:01:01.0001Z&end=2029-11-01T01:01:01.0001Z')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body.best_profession).toBeNull();
  });

  test('POST /admin/best-profession - date param error', async () => {
    const res = await request
      .get('/admin/best-profession?start=2025-01-01T01:01:01.0001Z')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(400);
    expect(res.body.error_code).toBe(ERROR_CODES.requestSchemaInvalid);
  });

  test('POST /admin/best-clients - success', async () => {
    const res = await request
      .get('/admin/best-clients?start=2018-01-01T01:01:01.0001Z&end=2023-11-01T01:01:01.0001Z&limit=3')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body).toHaveLength(3);
    expect(res.body[0].paid).toBeGreaterThanOrEqual(res.body[1].paid);
  });

  test('POST /admin/best-clients - no result success', async () => {
    const res = await request
      .get('/admin/best-clients?start=2025-01-01T01:01:01.0001Z&end=2029-11-01T01:01:01.0001Z')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(200);
    expect(res.body).toHaveLength(0);
  });

  test('POST /admin/best-clients - date param error', async () => {
    const res = await request
      .get('/admin/best-clients?start=2025-01-01T01:01:01.0001Z&end=20ssAA-01')
      .set({ profile_id: 3 })
      .send();

    expect(res.status).toBe(400);
    expect(res.body.error_code).toBe(ERROR_CODES.requestSchemaInvalid);
  });
});
